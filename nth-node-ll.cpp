/*
    Url : http://www.geeksforgeeks.org/write-a-function-to-get-nth-node-in-a-linked-list/
	function to get Nth node in a Linked List
*/

#include <iostream>
using namespace std;

struct node
{
	/* data */
	int data;
	node* next;
	node(int d, node* n)
	{
		data = d;
		next = n;
	}
};

node* getNth(node* head, int n)
{
	if(n <= 0) return NULL;
	else if(head == NULL) return NULL;
	else
	{
		while(n > 1)
		{
			head = head->next;
			if(head == NULL) break;
			n--;
		}
		return head;
	}
}

int main()
{
	node* n1 = new node(1, NULL);
	node* n2 = new node(2, n1);
	node* n3 = new node(3, n2);
	node* n4 = new node(4, n3);
	node* n5 = new node(5, n4);
	node* n6 = new node(6, n5);
	node* n7 = new node(7, n6);
	cout << getNth(n7, 2)->data << endl;
	return 0;
}