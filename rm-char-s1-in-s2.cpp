/*
	Url : http://www.geeksforgeeks.org/remove-characters-from-the-first-string-which-are-present-in-the-second-string/
	Remove characters from the first string which are present in the second string
*/

#include <iostream>
#include <string>
using namespace std;

void update(string s, int* a)
{
	for(int i=0; i<256; i++)
	{
		a[i] = 0;
	}
	for(unsigned int i=0; i<s.length(); i++)
	{
		a[(int)s[i]]++;
	}
}

string remove(string s, int * a)
{
	int j = 0;
	for(unsigned int i=0; i<s.length(); i++)
	{
		if(a[(int)s[i]] == 0)
		{
			s[j] = s[i];
			j++;
		}
	}
	s[j] = '\0';
	s = s.substr(0, j);
	return s;
}

int main()
{
	string s1 = "mask";
	string s2 = "geeksforgeeks";
	int* a = new int[256];
	update(s2, a);
	cout << remove(s1, a) << endl;
	delete[] a;
}
