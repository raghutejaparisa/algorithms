/*
	Url : http://www.geeksforgeeks.org/how-will-you-print-numbers-from-1-to-200-without-using-loop/
	print numbers from 1 to 100 without using loop
*/

#include <iostream>
using namespace std;

void print(int i)
{
	if(i > 100) return;
	else 
	{
		cout << i << " ";
		print(i+1);
	}
}

int main()
{
	print(1);
	cout << endl;
	return 0;
}
