/*
	Url : http://www.geeksforgeeks.org/write-a-c-program-to-calculate-powxn/
	program to calculate pow(x,y)
*/

#include <iostream>
using namespace std;

float power(float x, int y)
{
    float temp;
    if( y == 0)
       return 1;
    temp = power(x, y/2);
    if (y%2 == 0)
        return temp*temp;
    else
    {
        if(y > 0)
            return x*temp*temp;
        else
            return (temp*temp)/x;
    }
}

int main()
{
	float x = -2;
	int y = -3;
	cout << power(x, y) << endl;
}
