/*
	Url : http://www.geeksforgeeks.org/return-maximum-occurring-character-in-the-input-string/
	
	Write an efficient C function to return maximum occurring character in the input string e.g., if input string is test string then function should return t.
	
	Algorithm:

	Input string = “test”
	1: Construct character count array from the input string.
	  count['e'] = 1
	  count['s'] = 1
	  count['t'] = 2

	2: Return the index of maximum value in count array (returns ‘t’).
*/

#include <iostream>
#include <string>
using namespace std;

char max_occ_char(string s)
{
	int * a = new int[256];
	for(int i=0; i<256; i++)
	{
		a[i] = 0;
	}
	for(unsigned int i=0; i<s.length(); i++)
	{
		a[(int)s[i]]++;
	}
	char c;
	int count = -1;
	for(int i=0; i<256; i++)
	{
		if(a[i] > count)
		{
			count = a[i];
			c = (char) i;
		}
	}
	delete[] a;
	a = NULL;
	return c;
}

int main()
{
	string s = "test";
	cout << "Maximum occurring character in the input string is " << max_occ_char(s) << endl;
}
