/*
	Url : http://www.geeksforgeeks.org/print-all-the-duplicates-in-the-input-string/
	Print all the duplicates in the input string
*/
#include <iostream>
using namespace std;

int main()
{
	string s = "geekforgeeks";
	int * a = new int[256];
	for(int i=0; i<256; i++)
	{
		a[i] = 0;
	}
	for(unsigned int i=0; i<s.length(); i++)
	{
		a[(int)s[i]]++;
	}
	for(int i=0; i<256; i++)
	{
		if(a[i] > 1) cout << (char)i << " ";
	}
	cout << endl;
	return 0;
}
