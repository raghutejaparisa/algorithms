/*
	Url : http://www.geeksforgeeks.org/given-only-a-pointer-to-a-node-to-be-deleted-in-a-singly-linked-list-how-do-you-delete-it/
	Given only a pointer to a node to be deleted in a singly linked list, how do you delete it?
	*** Doesn't work if pointer is last of the linked list ***
*/

#include <iostream>
using namespace std;

struct node
{
	/* data */
	int data;
	node* next;
	node(int d, node* n)
	{
		data = d;
		next = n;
	}
};

void deleteptr(node* n)
{
	node* t = n->next;
	n->data = t->data;
	n->next = t->next;
	delete(t);
}

void printll(node* head)
{
	if(head == NULL) return;
	else
	{
		cout << head->data << " ";
		printll(head->next);
	}
}

int main()
{
	node* n1 = new node(1, NULL);
	node* n2 = new node(2, n1);
	node* n3 = new node(3, n2);
	node* n4 = new node(4, n3);
	node* n5 = new node(5, n4);
	node* n6 = new node(6, n5);
	node* n7 = new node(7, n6);
	deleteptr(n4);
	printll(n7);
	return 0;
}