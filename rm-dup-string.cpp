/*
    Url : http://www.geeksforgeeks.org/remove-all-duplicates-from-the-input-string/
    Remove all duplicates from the input string.
*/

#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main()
{
	string s = "geeksforgeeks";
	sort(s.begin(), s.end());
	char pre = s[0];
	int j = 1;
	for(unsigned int i=1; i<s.length(); i++)
	{
		if(s[i] != pre)
		{
			s[j] = s[i];
			j++;
			pre = s[i];
		}
	}
	s = s.substr(0, j);
	cout << s << endl;
}
