Algorithms
==========

Famous Algorithms and Its solutions such as Geek for Geeks

** It may contain the code same as the code in Geek for Geeks or some other sites...

** It's just for learning..


Filename (Url)

max-occ-char.cpp (http://www.geeksforgeeks.org/return-maximum-occurring-character-in-the-input-string/)

print-1-100.cpp (http://www.geeksforgeeks.org/how-will-you-print-numbers-from-1-to-200-without-using-loop/)

sum-of-digits.cpp (http://www.geeksforgeeks.org/how-can-we-sum-the-digits-of-a-given-number-in-single-statement/)

a-power-b.cpp (http://www.geeksforgeeks.org/write-a-c-program-to-calculate-powxn/)

rm-dup-string.cpp (http://www.geeksforgeeks.org/remove-all-duplicates-from-the-input-string/)
 
print-dup-string.cpp (http://www.geeksforgeeks.org/print-all-the-duplicates-in-the-input-string/)

nth-node-ll.cpp (http://www.geeksforgeeks.org/write-a-function-to-get-nth-node-in-a-linked-list/)

delete-ptr-ll.cpp (http://www.geeksforgeeks.org/given-only-a-pointer-to-a-node-to-be-deleted-in-a-singly-linked-list-how-do-you-delete-it/)

rm-char-s1-in-s2.cpp (http://www.geeksforgeeks.org/remove-characters-from-the-first-string-which-are-present-in-the-second-string/)

str-rotations.cpp (www.geeksforgeeks.org/a-program-to-check-if-strings-are-rotations-of-each-other-or-not/)
