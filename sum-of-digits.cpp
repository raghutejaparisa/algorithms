/*
	Url : http://www.geeksforgeeks.org/how-can-we-sum-the-digits-of-a-given-number-in-single-statement/
	sum the digits of a given number in single statement
*/

#include <iostream>
using namespace std;

int sum_of_digits(int n)
{
	int total;
	for(total=0; n>0; total+=n%10, n/=10);
	return total;
}

int main()
{
	cout << sum_of_digits(12345) << endl;
	return 0;
}

/*
	Alternative : Recursive
	
		int sumDigits(int no)
		{
			return no == 0 ? 0 : no%10 + sumDigits(no/10) ;
		}
*/
