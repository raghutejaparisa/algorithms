/*

	Url : http://www.geeksforgeeks.org/a-program-to-check-if-strings-are-rotations-of-each-other-or-not/
	A Program to check if strings are rotations of each other or not

	Given a string s1 and a string s2, write a snippet to say whether s2 is a rotation of s1 using only one call to strstr routine?
	(eg given s1 = ABCD and s2 = CDAB, return true, given s1 = ABCD, and s2 = ACBD , return false)

*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
	string s1 = "ABACD";
	string s2 = "CDABA";

	string temp = s1 + s1;
	
	size_t ptr;
	ptr = temp.find(s2);
	if((s1.length() == s2.length()) && (ptr != string::npos)) cout << "Yes" << endl;
	else cout << "NO" << endl;
	
	return 0;
}
